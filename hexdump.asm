global _start

section .text

_start:
	push rbp
	mov rbp, rsp

	sub rsp, 0x3
	mov rcx, rsp
	add rcx, 0x2
	mov [rcx], BYTE 0x20

read:	push rcx
	sub rcx, 0x2
	xor rax, rax	;sys_read
	xor rdi, rdi	;stdin
	mov rsi, rcx
	mov rdx, 0x1
	syscall
	pop rcx

	cmp rax, 0x1	;should only grab one byte
	jne exit

	mov rax, [rsp]
	mov rdx, rax

	and rax, 0xF0	;process top half of nibble
	ror rax, 0x4
	cmp rax, 0x9
	jg _A0_F0

	add rax, 0x30	;if 0x0 <= top_nibble <= 0x9
	mov [rsp], al	;0x30 == '0'
	jmp _0X

_A0_F0:	add rax, 0x37	;if top_nibble <= 0xA
	mov [rsp], al	;0x37 = 'A' - 0xA

_0X:	sub rcx, 0x1	;process bottom half of nibble
	mov rax, rdx
	and rax, 0xF
	cmp rax, 0x9
	jg _0A_0F

	add rax, 0x30	;if 0x0 <= bottom_nibble <= 0x9
	mov [rcx], al
	jmp write

_0A_0F:	add rax, 0x37	;if bottom_nibble <= 0xA
	mov [rcx], al

write:	sub rcx, 0x1
	push rcx
	mov rax, 0x1	;sys_write
	mov rdi, 0x1	;stdout
	mov rsi, rcx
	mov rdx, 0x3
	syscall
	pop rcx
	add rcx, 0x2
	jmp read

exit:	mov rsp, rbp
	xor rdi, rdi
	mov rax, 0x3C
	syscall
